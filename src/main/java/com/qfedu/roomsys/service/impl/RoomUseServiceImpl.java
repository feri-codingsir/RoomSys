package com.qfedu.roomsys.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.qfedu.roomsys.dao.RoomUseDao;
import com.qfedu.roomsys.dto.RoomUseAudit;
import com.qfedu.roomsys.entity.RoomUse;
import com.qfedu.roomsys.entity.User;
import com.qfedu.roomsys.service.intf.RoomUseService;
import com.qfedu.roomsys.vo.LayuiPageVo;
import com.qfedu.roomsys.vo.R;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.util.Date;

/**
 * ━━━━━━Feri出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　 ┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　  ┃
 * 　　┃　　　　　　 ┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃    邢哥的代码，怎么会，有bug呢，不可能！
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 * ━━━━━━永无BUG!━━━━━━
 *
 * @Description:
 * @Author：邢朋辉
 * @Date: 2023/9/7 15:18
 */
@Service
public class RoomUseServiceImpl implements RoomUseService {
    @Resource
    private RoomUseDao dao;

    @Override
    public R save(RoomUse use, HttpSession session) {
        User user= (User) session.getAttribute("user");
        if(user!=null){
            use.setStatus("未审核");
            use.setUid(user.getId());
            use.setCtime(new Date());
            if(dao.insert(use)>0){
                return new R(0,"OK",null);
            }
        }
        return new R(1,"FAIL",null);
    }

    @Override
    public R update(RoomUse use) {
        if(dao.updateById(use)>0){
            return new R(0,"OK",null);
        }
        return new R(1,"FAIL",null);
    }

    @Override
    public R audit(RoomUseAudit audit) {
       RoomUse use=new RoomUse();
       use.setId(audit.getId());
       use.setStatus(audit.getStatus());
       if(audit.getStatus().equals("拒绝")){
           use.setReject(audit.getReject());
       }
        if(dao.updateById(use)>0){
            return new R(0,"OK",null);
        }
        return new R(1,"FAIL",null);
    }

    @Override
    public R del(Integer id) {
        if(dao.deleteById(id)>0){
            return new R(0,"OK",null);
        }
        return new R(1,"FAIL",null);
    }

    @Override
    public LayuiPageVo queryPage(int page, int limit, HttpSession session) {
        User user= (User) session.getAttribute("user");
        QueryWrapper<RoomUse> wrapper = new QueryWrapper<>();
        if(user!=null) {
            if(user.getRole().equals("领导")){
                wrapper.eq("status","未审核");
            }
        }
        wrapper.orderByDesc("ctime");
        Page<RoomUse> roomUsePage=dao.selectPage(new Page<>(page,limit),wrapper);

        return new LayuiPageVo(roomUsePage.getTotal(),roomUsePage.getRecords());
    }
}
