package com.qfedu.roomsys.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.qfedu.roomsys.dao.UserDao;
import com.qfedu.roomsys.entity.User;
import com.qfedu.roomsys.service.intf.UserService;
import com.qfedu.roomsys.vo.R;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

/**
 * ━━━━━━Feri出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　 ┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　  ┃
 * 　　┃　　　　　　 ┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃    邢哥的代码，怎么会，有bug呢，不可能！
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 * ━━━━━━永无BUG!━━━━━━
 *
 * @Description:
 * @Author：邢朋辉
 * @Date: 2023/9/7 15:18
 */
@Service
public class UserServiceImpl implements UserService {
    @Resource
    private UserDao dao;

    @Override
    public R login(User user, HttpSession session) {
        //1.查询数据库
        QueryWrapper<User> wrapper=new QueryWrapper<>();
        wrapper.eq("name",user.getName()).eq("pass",user.getPass());
        wrapper.last( " limit 1");
        User u=dao.selectOne(wrapper);
        if(u!=null){
            //登录成功,记录当前成功的用户在Session（会话）
            session.setAttribute("user",u);

            return new R(0,"OK",u.getRole());
        }
        return new R(1,"FAIL",null);
    }

    @Override
    public R all() {
        return new R(0,"OK",dao.selectList(null));
    }
}
