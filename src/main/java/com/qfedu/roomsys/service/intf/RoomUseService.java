package com.qfedu.roomsys.service.intf;

import com.qfedu.roomsys.dto.RoomUseAudit;
import com.qfedu.roomsys.entity.RoomUse;
import com.qfedu.roomsys.vo.LayuiPageVo;
import com.qfedu.roomsys.vo.R;

import javax.servlet.http.HttpSession;

/**
 * ━━━━━━Feri出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　 ┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　  ┃
 * 　　┃　　　　　　 ┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃    邢哥的代码，怎么会，有bug呢，不可能！
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 * ━━━━━━永无BUG!━━━━━━
 *
 * @Description:
 * @Author：邢朋辉
 * @Date: 2023/9/7 15:16
 */
public interface RoomUseService {

    /**
     * 新增*/
    R save(RoomUse use, HttpSession session);
    /**
     * 修改*/
    R update(RoomUse use);
    /**
     * 审核*/
    R audit(RoomUseAudit audit);
    /**
     * 删除*/
    R del(Integer id);

    /**
     * 查询 分页 区分用户和领导*/
    LayuiPageVo queryPage(int page,int limit,HttpSession session);

}
