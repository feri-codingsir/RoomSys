package com.qfedu.roomsys.controller;

import com.qfedu.roomsys.dto.RoomUseAudit;
import com.qfedu.roomsys.entity.RoomUse;
import com.qfedu.roomsys.service.intf.RoomUseService;
import com.qfedu.roomsys.vo.LayuiPageVo;
import com.qfedu.roomsys.vo.R;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

/**
 * ━━━━━━Feri出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　 ┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　  ┃
 * 　　┃　　　　　　 ┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃    邢哥的代码，怎么会，有bug呢，不可能！
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 * ━━━━━━永无BUG!━━━━━━
 *
 * @Description:
 * @Author：邢朋辉
 * @Date: 2023/9/7 15:17
 */
@RestController
@Api(tags = "教室预约")
public class RoomUseController {
    @Resource
    private RoomUseService service;

    /**
     * 新增*/
    @PostMapping("save")
    public R save(RoomUse use, HttpSession session){
        return service.save(use,session);
    }

    /**
     * 修改*/
    @PostMapping("update")
    public R update(RoomUse use){
        return service.update(use);
    }
    /**
     * 审核*/
    @PostMapping("audit")
    public R audit(RoomUseAudit use){
        return service.audit(use);
    }
    /**
     * 删除*/
    @GetMapping("del")
    public R del(Integer id){
        return service.del(id);
    }

    /**
     * 查询 分页 区分用户和领导*/
    @GetMapping("page")
    public LayuiPageVo page(int page,int limit,HttpSession session){
        return service.queryPage(page, limit, session);
    }
}
